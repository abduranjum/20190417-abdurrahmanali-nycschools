//
//  AppDelegate.h
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/16/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

