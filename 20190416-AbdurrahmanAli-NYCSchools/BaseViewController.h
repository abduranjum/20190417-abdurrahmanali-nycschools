//
//  BaseViewController.h
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/17/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UITableViewController

- (void)fetchDataFromUrlWithString:(NSString *)urlString andSuccessBlock:(void (^)(NSData *data))successBlock;

@end
