//
//  DetailViewController.m
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/16/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import "DetailViewController.h"
#import "SubDetailViewController.h"

@interface DetailViewController ()

@property (strong, nonatomic) NSDictionary *additionalInformationDTO;
@property (strong, nonatomic) NSArray *sortedKeysForAdditionalInformationDTO;
@property (strong, nonatomic) NSArray *sortedKeysForBasicInformationDTO;

@end

@implementation DetailViewController

#pragma mark - View Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showSubDetail"])
    {
        NSString *key = [(UITableViewCell *)sender textLabel].text;
        NSString *value = [(UITableViewCell *)sender detailTextLabel].text;

        SubDetailViewController *controller = (SubDetailViewController *)[segue.destinationViewController topViewController];
        
        controller.title = key;
        controller.value = value;
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Helper Methods

- (void)setSchoolDTO:(NSDictionary *)newDetailItem
{
    if (_schoolDTO != newDetailItem)
    {
        _schoolDTO = newDetailItem;
        
        [self configureView];
    }
}

- (void)configureView
{
    self.title = self.schoolDTO[@"school_name"];
    
    if (self.schoolDTO.allKeys.count)
    {
        self.sortedKeysForBasicInformationDTO = [self.schoolDTO.allKeys sortedArrayUsingSelector:@selector(compare:)];
    }
    else
    {
        self.sortedKeysForBasicInformationDTO = @[@"no_data_available"];
    }
    
    [self fetchSATScores];
}

- (void)fetchSATScores
{
    NSString *urlString = [NSString stringWithFormat:@"https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=%@", self.schoolDTO[@"dbn"]];

    [self fetchDataFromUrlWithString:urlString andSuccessBlock:^(NSData *data)
     {
         NSError *parseError = Nil;
         NSArray<NSDictionary *> *additionalInformationDTOs = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
         
         self.additionalInformationDTO = additionalInformationDTOs.firstObject;
         
         if (self.additionalInformationDTO.allKeys.count)
         {
             NSMutableArray<NSDictionary *> *keysForAdditionalInformationDTO = self.additionalInformationDTO.allKeys.mutableCopy;
             
             [keysForAdditionalInformationDTO removeObjectsInArray:self.sortedKeysForBasicInformationDTO];
             
             self.sortedKeysForAdditionalInformationDTO = [keysForAdditionalInformationDTO sortedArrayUsingSelector:@selector(compare:)];
         }
         else
         {
             self.sortedKeysForAdditionalInformationDTO = @[@"no_data_available"];
         }
     }];
}

- (void)refreshInvoked:(id)sender forState:(UIControlState)state
{
    [self fetchSATScores];
}

#pragma mark - Table View DataSource & Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *titleForSectionHeader;
    
    if (section == 0)
    {
        titleForSectionHeader = @"Additional Information";
    }
    else
    {
        titleForSectionHeader = @"Basic Information";
    }
    
    return titleForSectionHeader;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfSectionRows = 0;
    
    if (section == 0)
    {
        numberOfSectionRows = self.sortedKeysForAdditionalInformationDTO.count;
    }
    else
    {
        numberOfSectionRows = self.sortedKeysForBasicInformationDTO.count;
    }
    
    return numberOfSectionRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell" forIndexPath:indexPath];
    NSString *text;
    NSString *detailText;

    if (indexPath.section == 0)
    {
        NSString *additionalInformationDTOKey = self.sortedKeysForAdditionalInformationDTO[indexPath.row];
        
        text = additionalInformationDTOKey;
        detailText = self.additionalInformationDTO[additionalInformationDTOKey];
    }
    else
    {
        NSString *basicInformationDTOKey = self.sortedKeysForBasicInformationDTO[indexPath.row];

        text = basicInformationDTOKey;
        detailText = self.schoolDTO[basicInformationDTOKey];
    }
    
    text = [self getTitleCaseStringFromSnakeCaseString:text];
    
    cell.textLabel.text = text;
    cell.detailTextLabel.text = detailText;
    
    return cell;
}

- (NSString *)getTitleCaseStringFromSnakeCaseString:(NSString *)snakeCaseString
{
    NSString *titleCaseString = [snakeCaseString stringByReplacingOccurrencesOfString:@"_" withString:@" "].capitalizedString;
    
    return titleCaseString;
}

@end
