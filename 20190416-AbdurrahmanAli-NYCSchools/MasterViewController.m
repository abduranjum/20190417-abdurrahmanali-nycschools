//
//  MasterViewController.m
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/16/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()

@property (strong, nonatomic) NSArray<NSDictionary *> *sortedSchoolDTOs;

@property (strong, nonatomic) DetailViewController *detailViewController;

@end

@implementation MasterViewController

#pragma mark - View Life Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"NYC High Schools";

    self.detailViewController = (DetailViewController *)[self.splitViewController.viewControllers.lastObject topViewController];
    
    [self fetchNYCSchools];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    
    [super viewWillAppear:animated];
}

#pragma mark - Helper Methods

- (void)fetchNYCSchools
{
    NSString *urlString = @"https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
    
    [self fetchDataFromUrlWithString:urlString andSuccessBlock:^(NSData *data)
     {
         NSError *parseError = nil;
         
         NSArray<NSDictionary *> *schoolDTOs = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
         
         NSSortDescriptor *schoolNameSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"school_name" ascending:YES];
         
         self.sortedSchoolDTOs = [schoolDTOs sortedArrayUsingDescriptors:@[schoolNameSortDescriptor]];
     }];
}

- (void)refreshInvoked:(id)sender forState:(UIControlState)state
{    
    [self fetchNYCSchools];
}

#pragma mark - Segue Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDetail"])
    {
        DetailViewController *detailViewController = (DetailViewController *)[segue.destinationViewController topViewController];
       
        detailViewController.schoolDTO = self.sortedSchoolDTOs[self.tableView.indexPathForSelectedRow.row];
        detailViewController.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        detailViewController.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View DataSource & Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sortedSchoolDTOs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSDictionary *schoolDTO = self.sortedSchoolDTOs[indexPath.row];
    
    cell.textLabel.text = schoolDTO[@"school_name"];
    cell.detailTextLabel.text = schoolDTO[@"primary_address_line_1"];

    return cell;
}

@end
