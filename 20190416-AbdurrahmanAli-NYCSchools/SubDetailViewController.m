//
//  SubDetailViewController.m
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/17/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import "SubDetailViewController.h"

@interface SubDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end

@implementation SubDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.valueLabel.text = self.value;
}

@end
