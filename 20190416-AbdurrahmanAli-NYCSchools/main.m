//
//  main.m
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/16/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
