//
//  BaseViewController.m
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/17/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeActivityIndicator];
}

- (void)initializeActivityIndicator
{
    self.refreshControl = [UIRefreshControl new];
    
    [self.refreshControl addTarget:self action:@selector(refreshInvoked:forState:) forControlEvents:UIControlEventValueChanged];
}

- (void)fetchDataFromUrlWithString:(NSString *)urlString andSuccessBlock:(void (^)(NSData *data))successBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest new];

    request.URL = [NSURL URLWithString:urlString];
    request.HTTPMethod = @"GET";

    NSURLSession *session = [NSURLSession sharedSession];

    [self.refreshControl beginRefreshing];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
      {
          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;

          UIAlertController *alertViewController;

          if (httpResponse.statusCode == 200)
          {
              if (successBlock)
              {
                  successBlock(data);
              }
          }
          else
          {
              alertViewController = [UIAlertController alertControllerWithTitle:@"Alert" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];

              UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                       {
                                           [alertViewController dismissViewControllerAnimated:YES completion:Nil];
                                       }];

              [alertViewController addAction:cancel];
          }

          dispatch_async
          (dispatch_get_main_queue(), ^
           {
               if (alertViewController)
               {
                   [self presentViewController:alertViewController animated:YES completion:Nil];
               }
               else
               {
                   [self.tableView reloadData];
               }

               [self.refreshControl endRefreshing];
           });
      }] resume];
}

- (void)refreshInvoked:(id)sender forState:(UIControlState)state
{

}

@end
