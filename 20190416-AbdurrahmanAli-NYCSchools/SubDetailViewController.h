//
//  SubDetailViewController.h
//  20190416-AbdurrahmanAli-NYCSchools
//
//  Created by Abdurrahman Mubeen Ali on 4/17/19.
//  Copyright © 2019 Abdurrahman Mubeen Ali. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubDetailViewController : UIViewController

@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *value;

@end
